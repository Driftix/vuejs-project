export default interface History{
    nom: string,
    id: string,
    score: number,
}