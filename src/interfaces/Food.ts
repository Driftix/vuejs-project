export default interface Food{
    description: string,
    fdcId: string,
    score:number,
    servingSize: number,
    servingSizeUnit: string
    
}