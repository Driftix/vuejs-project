import type Nutriment from "./Nutriment";
//à modifier car any correspond aux nutriments dans foodnutrients (.nutrient)
export default interface Consommation{
    nom: string,
    score:number,
    nutriments : any[]
}