import { createRouter, createWebHistory } from 'vue-router'
import Search from '../views/Search.vue'
import Details from '../views/Details.vue'
import Historique from '../views/Historique.vue'
import Consommation from '../views/Consommation.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Search
    },
    {
      path:'/details?id:identifiant&score:score',
      name: "details",
      component: Details,
      props:true
    },
    {
      path:"/historique",
      name:"historique",
      component: Historique
    },
    {
      path:"/consommation",
      name:"consommation",
      component: Consommation,
    }
  ]
})
export default router
