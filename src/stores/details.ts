import { defineStore } from 'pinia'
import axios from 'axios'
import type History from '../interfaces/History'

export const useDetailsStore = defineStore('details', {
  state: () => {
    return { 
        history: [] as History[],
        actualResult : Object as any,
     }
  },
  actions: {
    async get(id:string, nutriScore:number){
        const resp = await axios.get(`https://api.nal.usda.gov/fdc/v1/food/${id}?api_key=Clc1gTYI1KaIAAY5sESrzahhRyLiWxHPaHSoEQiC`);
        this.actualResult = resp.data
        
        const produit: History = {
            nom: this.actualResult.description,
            id: id,
            score: nutriScore
        }
        this.history.push(produit)
    }
  },
})