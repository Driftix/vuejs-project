import { defineStore } from 'pinia'
import axios from 'axios'
import type Nutriment from '../interfaces/Nutriment'
import type Consommation from '@/interfaces/Consommation'

export const useConsommationStore = defineStore('consommation', {
  state: () => {
    return { 
        produits: [] as Consommation[],
     }
  },
  actions: {
    async add(id:string,score:number){
        const resp = await axios.get(`https://api.nal.usda.gov/fdc/v1/food/${id}?api_key=Clc1gTYI1KaIAAY5sESrzahhRyLiWxHPaHSoEQiC`);
        const produit: Consommation = {
          nom: resp.data.description,
          score : score,
          nutriments : resp.data.foodNutrients
      }
        this.produits.push(produit)
    }
  },
})